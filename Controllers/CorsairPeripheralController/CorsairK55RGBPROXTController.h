/*--------------------------------------------*\
|  CorsairK55RGBPROXTController.h              |
|                                              |
|  Driver for Corsair K55 RGB PRO XT Keyboard  |
\*--------------------------------------------*/

#ifndef CORSAIRK55RGBPROXTCONTROLLER_H
#define CORSAIRK55RGBPROXTCONTROLLER_H

#include "RGBController.h"

#include <string>
#include <hidapi/hidapi.h>


class CorsairK55RGBPROXTController
{
public:
    CorsairK55RGBPROXTController(hid_device* dev_handle, const char* path);
    ~CorsairK55RGBPROXTController();
    std::string GetDeviceLocation();
    std::string GetFirmwareString();
    std::string GetName();
    std::string GetSerialString();

    void        SetLEDs(std::vector<RGBColor> colors);
    void        SetName(std::string device_name);

private:
    hid_device* dev;

    std::string firmware_version;
    std::string location;
    std::string name;
    device_type type;

    void        LightingControl();
};

#endif // CORSAIRK55RGBPROXTCONTROLLER_H
