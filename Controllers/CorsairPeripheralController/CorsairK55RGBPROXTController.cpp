/*--------------------------------------------*\
|  CorsairK55RGBPROXTController.cpp            |
|                                              |
|  Driver for Corsair K55 RGB PRO XT Keyboard  |
\*--------------------------------------------*/

#include "CorsairK55RGBPROXTController.h"
#include "LogManager.h"


#define COLOR_BANK_SIZE     137


static const unsigned int keys[] = {127, 128, 129, 130, 131, 132,  37,  49,  39,  53, 102, 101,  26,  96, 104,  54,  27,  16,   0,  25,
                                    103,  55,  28,  22,  18,  23,  56,  29,   4,   3,   2,  57,  30,  17,   5,  21,  31,  58,  32,  19,
                                      6,   1,  40,  59,  33,  24,   7,  60,  34,  20,   9,  13,  61,  35,   8,  10,  12,  14,  11,  50,
                                     62,  41,  15,  47,  51, 107,  63,  42,  43,  48,  52, 118,  64,  38,  44,  46, 106,  97,  65,  36,
                                    105,  66,  69,  72,  76,  67,  70,  73,  78,  77,  68,  71,  74,  75,  79,  91,  88,  85,  94,  80,
                                     92,  89,  86,  81,  93,  90,  87,  95,  82,  83,  84 };


static unsigned char color_bank[3][COLOR_BANK_SIZE];


CorsairK55RGBPROXTController::CorsairK55RGBPROXTController(hid_device* dev_handle, const char* path)
{
    dev             = dev_handle;
    location        = path;

    LightingControl();
}

CorsairK55RGBPROXTController::~CorsairK55RGBPROXTController()
{
    hid_close(dev);
}

std::string CorsairK55RGBPROXTController::GetDeviceLocation()
{
    return("HID: " + location);
}

std::string CorsairK55RGBPROXTController::GetFirmwareString()
{
    return "";
}

std::string CorsairK55RGBPROXTController::GetName()
{
    return name;
}

void CorsairK55RGBPROXTController::SetName(std::string device_name)
{
    name = device_name;
}

std::string CorsairK55RGBPROXTController::GetSerialString()
{
    wchar_t serial_string[128];
    int ret = hid_get_serial_number_string(dev, serial_string, 128);

    if(ret != 0)
    {
        return("");
    }

    std::wstring return_wstring = serial_string;
    std::string return_string(return_wstring.begin(), return_wstring.end());

    return(return_string);
}

void CorsairK55RGBPROXTController::LightingControl()
{
    unsigned char usb_buf[65];

    memset(usb_buf, 0x00, sizeof(usb_buf));
    usb_buf[0x01] = 0x08;
    usb_buf[0x02] = 0x01;
    usb_buf[0x03] = 0x03;
    usb_buf[0x05] = 0x02;
    hid_write(dev, (unsigned char *)usb_buf, 65);

    memset(usb_buf, 0x00, sizeof(usb_buf));
    usb_buf[0x01] = 0x08;
    usb_buf[0x02] = 0x02;
    usb_buf[0x03] = 0x5F;
    hid_write(dev, (unsigned char *)usb_buf, 65);
	
    memset(usb_buf, 0x00, sizeof(usb_buf));
    usb_buf[0x01] = 0x08;
    usb_buf[0x02] = 0x0D;
    usb_buf[0x04] = 0x01;
    hid_write(dev, (unsigned char *)usb_buf, 65);
}

void CorsairK55RGBPROXTController::SetLEDs(std::vector<RGBColor>colors)
{
    for(std::size_t color_idx = 0; color_idx < colors.size(); ++color_idx)
    {
        RGBColor color = colors[color_idx];
        color_bank[0][keys[color_idx]] = RGBGetRValue(color);
        color_bank[1][keys[color_idx]] = RGBGetGValue(color);
        color_bank[2][keys[color_idx]] = RGBGetBValue(color);
    }

    unsigned char* color_ptr = &color_bank[0][0];
    unsigned char usb_buf[65];
    memset(usb_buf, 0x00, sizeof(usb_buf));

    usb_buf[0x01] = 0x08;
    usb_buf[0x02] = 0x06;
    usb_buf[0x04] = 0x9B;
    usb_buf[0x05] = 0x01;

    memcpy(&usb_buf[12], color_ptr, 53);
    color_ptr += 53;
    hid_write(dev, (unsigned char *)usb_buf, 65);

    usb_buf[0x02] = 0x07;

    for(std::size_t i = 0; i < 6; ++i)
    {
        memcpy(&usb_buf[4], color_ptr, 61);
        color_ptr += 61;
        hid_write(dev, (unsigned char *)usb_buf, 65);
    }
}
